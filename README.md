# Google Maps Services

This module integrates Google Maps Services with Drupal.
This module provides a service that provides access
for the following Google Maps APIs:

* [Directions API]
* [Distance Matrix API]
* [Elevation API]
* [Geocoding API]
* [Geolocation API]
* [Time Zone API]

## Install Module
```
composer require drupal/google_maps_services
drush en google_maps_services
```

## API keys
Each Google Maps Web Service request requires an API key.
Learn how to [Get the API key].

## Configure the Module
* Visit `/admin/config/services/google-maps-services`
* Add the API key

## Usage examples

### Endpoint manager
```php
$endpoint_manager = \Drupal::service('google_maps_services.api.endpoint_manager');
```

### Directions API
```php
$directions = $endpoint_manager->getEndpoint('directions');
$origins = '51.476795,-0.000531';
$destinations = '51.495796,-0.108640';
$result = $directions->getDirections($origins, $destinations);
```

### Distance Matrix API
```php
$distance_matrix = $endpoint_manager->getEndpoint('distance_matrix');
$origins = '51.476795,-0.000531';
$destinations = '51.495796,-0.108640';
$params = [
  'mode' => 'transit',
];
$result = $distance_matrix->getDistanceMatrix($origins, $destinations, $params);
```

### Elevation API
```php
$elevation = $endpoint_manager->getEndpoint('elevation');
$result = $elevation->getElevation('51.476795,-0.000531');
```

### Geocoding API
```php
$geocoding = $endpoint_manager->getEndpoint('geocoding');
$geocode_result = $geocoding->geocode('Royal Observatory Greenwich');
$reverse_geocode = $geocoding->reverseGeocode(['51.476795', '-0.000531']);
$placeid_result = $geocoding->reverseGeocodePlaceId('ChIJp9ypjCqo2EcRAoQcRV-yqzE');
```

### Geolocation API
```php
$geolocation = $endpoint_manager->getEndpoint('geolocation');
$result = $geolocation->geolocate();
```

### Time Zone API
```php
$timezone = $endpoint_manager->getEndpoint('timezone');
$result = $timezone->getTimezone(['51.476795', '-0.000531']);
```

[Get the API key]: https://developers.google.com/maps/documentation/javascript/get-api-key
[Directions API]: https://developers.google.com/maps/documentation/directions/
[Distance Matrix API]: https://developers.google.com/maps/documentation/distancematrix/
[Elevation API]: https://developers.google.com/maps/documentation/elevation/
[Geocoding API]: https://developers.google.com/maps/documentation/geocoding/
[Geolocation API]: https://developers.google.com/maps/documentation/geolocation/
[Time Zone API]: https://developers.google.com/maps/documentation/timezone/
