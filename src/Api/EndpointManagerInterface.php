<?php

namespace Drupal\google_maps_services\Api;

/**
 * Interface EndpointManagerInterface.
 *
 * @package Drupal\google_maps_services\Api
 */
interface EndpointManagerInterface {

  /**
   * Collect tagged endpoint service.
   */
  public function addEndpoint(EndpointInterface $endpoint);

  /**
   * Get endpoint by name.
   *
   * @param string $endpoint_name
   *   Endpoint name.
   *
   * @return \Drupal\google_maps_services\Api\EndpointInterface
   *   Endpoint service.
   */
  public function getEndpoint($endpoint_name);

  /**
   * Get list of endpoints.
   *
   * @return \Drupal\google_maps_services\Api\EndpointInterface[]
   *   List of endpoint services.
   */
  public function getEndpoints();

}
