<?php

namespace Drupal\google_maps_services\Api;

/**
 * Class AbstractEndpoint.
 *
 * @package Drupal\google_maps_services\Api
 */
abstract class AbstractEndpoint implements EndpointInterface {

  /**
   * API Client.
   *
   * @var \Drupal\google_maps_services\Api\ClientInterface
   */
  protected $client;

  /**
   * AbstractEndpoint constructor.
   *
   * @param \Drupal\google_maps_services\Api\ClientInterface $client
   *   The client service.
   */
  public function __construct(ClientInterface $client) {
    $this->client = $client;
  }

}
