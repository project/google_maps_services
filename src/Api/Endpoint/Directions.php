<?php

namespace Drupal\google_maps_services\Api\Endpoint;

use Drupal\google_maps_services\Api\AbstractEndpoint;

/**
 * Directions Service.
 *
 * @package Drupal\google_maps_services\Api\Endpoint
 *
 * @see https://developers.google.com/maps/documentation/directions/
 */
class Directions extends AbstractEndpoint {

  const API_PATH = 'maps/api/directions/json';

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'directions';
  }

  /**
   * Calculates directions between locations.
   *
   * @param string $origin
   *   Origin.
   * @param string $destination
   *   Destination.
   * @param array $params
   *   Parameters.
   * @param bool $cacheable
   *   Cacheable.
   *
   * @return array|bool
   *   Result array.
   */
  public function getDirections($origin, $destination, array $params = [], $cacheable = TRUE) {
    $params['origin'] = (string) $origin;
    $params['destination'] = (string) $destination;

    $result = $this->client->request(self::API_PATH, $params, $cacheable);

    if ($result) {
      return $result;
    }

    return FALSE;
  }

}
