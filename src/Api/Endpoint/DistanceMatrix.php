<?php

namespace Drupal\google_maps_services\Api\Endpoint;

use Drupal\google_maps_services\Api\AbstractEndpoint;

/**
 * Distance Matrix Service.
 *
 * @package Drupal\google_maps_services\Api\Endpoint
 *
 * @see https://developers.google.com/maps/documentation/distance-matrix/
 */
class DistanceMatrix extends AbstractEndpoint {

  const API_PATH = 'maps/api/distancematrix/json';

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'distance_matrix';
  }

  /**
   * Provides travel distance and time for a matrix of origins and destinations.
   *
   * @param string $origins
   *   Origins.
   * @param string $destinations
   *   Destinations.
   * @param array $params
   *   Parameters.
   * @param bool $cacheable
   *   Cacheable.
   *
   * @return array|bool
   *   Result array.
   */
  public function getDistanceMatrix($origins, $destinations, array $params = [], $cacheable = TRUE) {
    $params['origins'] = (string) $origins;
    $params['destinations'] = (string) $destinations;

    $result = $this->client->request(self::API_PATH, $params, $cacheable);

    if ($result) {
      return $result;
    }

    return FALSE;
  }

}
