<?php

namespace Drupal\google_maps_services\Api\Endpoint;

use Drupal\google_maps_services\Api\AbstractEndpoint;

/**
 * Elevation Service.
 *
 * @package Drupal\google_maps_services\Api\Endpoint
 *
 * @see https://developers.google.com/maps/documentation/elevation/
 */
class Elevation extends AbstractEndpoint {

  const API_PATH = 'maps/api/elevation/json';

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'elevation';
  }

  /**
   * Provides elevation data for locations on the earth.
   *
   * @param string $locations
   *   Locations.
   * @param array $params
   *   Parameters.
   * @param bool $cacheable
   *   Cacheable.
   *
   * @return array|bool
   *   Result array.
   */
  public function getElevation($locations, array $params = [], $cacheable = TRUE) {
    // 'locations' seems to only allow 'lat,lng' pattern.
    if (is_string($locations)) {
      $params['locations'] = $locations;
    }
    else {
      list($lat, $lng) = $locations;
      $params['locations'] = "{$lat},{$lng}";
    }

    $result = $this->client->request(self::API_PATH, $params, $cacheable);

    if ($result) {
      return $result;
    }

    return FALSE;
  }

}
