<?php

namespace Drupal\google_maps_services\Api\Endpoint;

use Drupal\google_maps_services\Api\AbstractEndpoint;

/**
 * Timezone Service.
 *
 * @package Drupal\google_maps_services\Api\Endpoint
 *
 * @see https://developers.google.com/maps/documentation/timezone/
 */
class Timezone extends AbstractEndpoint {

  const API_PATH = 'maps/api/timezone/json';

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'timezone';
  }

  /**
   * Provides a simple interface to request the time zone for locations.
   *
   * @param string $location
   *   Location.
   * @param string $timestamp
   *   Timestamp.
   * @param array $params
   *   Parameters.
   * @param bool $cacheable
   *   Cacheable.
   *
   * @return array|bool
   *   Result array.
   */
  public function getTimezone($location, $timestamp = NULL, array $params = [], $cacheable = TRUE) {
    if (is_string($location)) {
      $params['location'] = $location;
    }
    else {
      list($lat, $lng) = $location;
      $params['location'] = "{$lat},{$lng}";
    }

    if (empty($timestamp)) {
      $params['timestamp'] = time();
    }

    $result = $this->client->request(self::API_PATH, $params, $cacheable);

    if ($result) {
      return $result;
    }

    return FALSE;
  }

}
