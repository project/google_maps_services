<?php

namespace Drupal\google_maps_services\Api\Endpoint;

use Drupal\google_maps_services\Api\AbstractEndpoint;

/**
 * Geolocation Service.
 *
 * @package Drupal\google_maps_services\Api\Endpoint
 *
 * @see https://developers.google.com/maps/documentation/geolocation/
 */
class Geolocation extends AbstractEndpoint {

  const API_PATH = 'https://www.googleapis.com/geolocation/v1/geolocate';

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'geolocation';
  }

  /**
   * Get location data from cell towers and WiFi nodes.
   *
   * @param array $bodyParams
   *   Request body.
   * @param bool $cacheable
   *   Cacheable.
   *
   * @return array|bool
   *   Result array.
   */
  public function geolocate(array $bodyParams = [], $cacheable = TRUE) {

    $result = $this->client->request(self::API_PATH, [], $cacheable, 'POST', $bodyParams);

    if ($result) {
      return $result;
    }

    return FALSE;
  }

}
