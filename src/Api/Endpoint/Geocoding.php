<?php

namespace Drupal\google_maps_services\Api\Endpoint;

use Drupal\google_maps_services\Api\AbstractEndpoint;

/**
 * Geocoding Service.
 *
 * @package Drupal\google_maps_services\Api\Endpoint
 *
 * @see https://developers.google.com/maps/documentation/geocoding/
 */
class Geocoding extends AbstractEndpoint {

  const API_PATH = 'maps/api/geocode/json';

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'geocoding';
  }

  /**
   * Geocode.
   *
   * @param string $address
   *   Address.
   * @param array $params
   *   Parameters.
   * @param bool $cacheable
   *   Cacheable.
   *
   * @return array|bool
   *   Result array.
   */
  public function geocode($address, array $params = [], $cacheable = TRUE) {
    $params['address'] = (string) $address;

    $result = $this->client->request(self::API_PATH, $params, $cacheable);

    if ($result) {
      return $result;
    }

    return FALSE;
  }

  /**
   * Reverse Geocode.
   *
   * @param array $latlng
   *   The latitude and longitude values.
   * @param array $params
   *   Parameters.
   * @param bool $cacheable
   *   Cacheable.
   *
   * @return array|bool
   *   Result array.
   */
  public function reverseGeocode(array $latlng, array $params = [], $cacheable = TRUE) {
    list($lat, $lng) = $latlng;
    $params['latlng'] = "{$lat},{$lng}";

    $result = $this->client->request(self::API_PATH, $params, $cacheable);

    if ($result) {
      return $result;
    }

    return FALSE;
  }

  /**
   * Retrieving an address for a Place ID.
   *
   * @param string $place_id
   *   The place ID.
   * @param array $params
   *   Parameters.
   * @param bool $cacheable
   *   Cacheable.
   *
   * @return array|bool
   *   Result array.
   */
  public function reverseGeocodePlaceId($place_id, array $params = [], $cacheable = TRUE) {
    $params['place_id'] = (string) $place_id;

    $result = $this->client->request(self::API_PATH, $params, $cacheable);

    if ($result) {
      return $result;
    }

    return FALSE;
  }

}
