<?php

namespace Drupal\google_maps_services\Api;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface as HttpClientInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\Exception\RequestException;
use Drupal\Component\Serialization\Json;

/**
 * Class HttpClient.
 *
 * @package Drupal\google_maps_services\Api\Client
 */
class HttpClient implements ClientInterface {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The Google Maps API key.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * The base API host.
   *
   * @var string
   */
  protected $apiBaseUri;

  /**
   * The length of time for which a cached item should be valid.
   *
   * @var string
   */
  protected $apiCacheMaxAge;

  /**
   * Client constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger interface.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   The cache backend.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              HttpClientInterface $http_client,
                              LoggerChannelInterface $logger,
                              CacheBackendInterface $cacheBackend) {
    $this->config = $config_factory->get('google_maps_services.settings');
    $this->httpClient = $http_client;
    $this->logger = $logger;
    $this->cacheBackend = $cacheBackend;

    $this->apiKey = $this->config->get('api_key');
    $this->apiBaseUri = $this->config->get('api_base_uri');
    $this->apiCacheMaxAge = $this->config->get('api_cache_maximum_age');
  }

  /**
   * {@inheritdoc}
   */
  public function request($apiPath, array $params = [], $cacheable = TRUE, $method = 'GET', array $body = []) {
    $hash = $this->buildArgHash($apiPath, $params, $method, $body);
    $cid = 'google_maps_services:' . $hash;

    // Check cache.
    if ($cache = $this->cacheBackend->get($cid)) {
      $result = $cache->data;
    }
    else {
      $result = $this->doRequest($apiPath, $params, $method, $body);
      if (!empty($result) && empty($result['error_message'])) {
        // Cache the response if we got one.
        if ($this->apiCacheMaxAge != 0 && $cacheable == TRUE) {
          $cache_time = time() + $this->apiCacheMaxAge;
          $this->cacheBackend->set($cid, $result, $cache_time);
        }
      }

    }
    return $result;
  }

  /**
   * HTTP request to Google Maps Services.
   *
   * @param string $apiPath
   *   API path.
   * @param array $params
   *   Parameters.
   * @param string $method
   *   Request method.
   * @param array $body
   *   Request body.
   *
   * @return array
   *   Response.
   */
  private function doRequest($apiPath, array $params, $method, array $body) {
    if (empty($this->apiKey)) {
      $message = $this->t('API key is not set.');
      $this->logger->error($message);
      return ['error_message' => $message];
    }

    // Guzzle request options.
    $options = [
      'http_errors' => FALSE,
      'base_uri' => $this->apiBaseUri,
    ];

    // Parameters for Auth.
    $defaultParams = [
      'key' => $this->apiKey,
    ];

    // Query.
    $options['query'] = array_merge($defaultParams, $params);

    // Body.
    if ($body) {
      $options['body'] = JSON::encode($body);
    }

    try {
      $response = $this->httpClient->request($method, $apiPath, $options);
      $result = $response->getBody()->getContents();
      $result = Json::decode($result);

      if (isset($result['error_message'])) {
        $this->logger->error($result['error_message']);
      }
      elseif ($response->getStatusCode() != 200) {
        $message = $this->t('The Google Maps API could not be successfully reached.');
        $this->logger->error($message);
        $result['error_message'] = $message;
      }

      return $result;
    }
    catch (RequestException $exception) {
      $message = $this->t('The Google Maps API could not be successfully reached. Message: @message', [
        '@message' => $exception->getMessage(),
      ]);

      $this->logger->error($message);
      return ['error_message' => $message];
    }

  }

  /**
   * Build Hash from Args array.
   *
   * @param string $apiPath
   *   API path.
   * @param array $params
   *   Parameters.
   * @param string $method
   *   Request method.
   * @param array $body
   *   Request body.
   *
   * @return string
   *   Hash.
   */
  private function buildArgHash($apiPath, array $params, $method, array $body) {
    // Build a hash from a function's argument list.
    // We will also use it for the cache id.
    $hash = hash('sha256', serialize(func_get_args()));
    return $hash;
  }

}
