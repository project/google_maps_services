<?php

namespace Drupal\google_maps_services\Api;

/**
 * Interface ClientInterface.
 *
 * @package Drupal\google_maps_services\Api
 */
interface ClientInterface {

  /**
   * Request Google Maps API data.
   *
   * @param string $apiPath
   *   API path.
   * @param array $params
   *   Parameters.
   * @param bool $cacheable
   *   Should the request be cached or not.
   * @param string $method
   *   Request method.
   * @param array $body
   *   Request body.
   *
   * @return array
   *   Result.
   */
  public function request($apiPath, array $params = [], $cacheable = TRUE, $method = 'GET', array $body = []);

}
