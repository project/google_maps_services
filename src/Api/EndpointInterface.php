<?php

namespace Drupal\google_maps_services\Api;

/**
 * Interface EndpointInterface.
 *
 * @package Drupal\google_maps_services\Api
 */
interface EndpointInterface {

  /**
   * Script to fetch from the API.
   *
   * @return string
   *   Endpoint name.
   */
  public function getName();

}
